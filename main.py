from json import loads as json_loads
from urequests import get as http_get, put as http_put
from wifimgr import get_connection


wlan = get_connection()
if wlan is None:
    print("Could not initialize the network connection.")
    while True:
        pass  # you shall not pass :D
print("Connected")
# Get state
state = json_loads(
        http_get('http://192.168.0.18/api/JjW34uaRV0F7pnR4J4nno3yOdICGnxhxKD0npsKq/groups/1').content
    ).get('state').get('all_on')

# Toggle lights
response = http_put(
        'http://192.168.0.18/api/JjW34uaRV0F7pnR4J4nno3yOdICGnxhxKD0npsKq/groups/1/action',
        json={"on": not state}
    )
