# Purpose
This is a proof of concept that demonstrates how powerful the ESP8266 microchip is.

# Getting started
1. Get an [ESP8266](https://www.aliexpress.com/item/1pcs-ESP8266-esp-01-remote-serial-Port-WIFI-wireless-module-through-walls-best-board/32871115651.html) with [USB adapter](https://www.aliexpress.com/item/OPEN-SMART-USB-to-ESP8266-ESP-01-Wi-Fi-Adapter-Module-w-CH340G-Driver/32795568611.html), that has switch for UART/PROG mode (this is not intended to be an advertiesment, but shows examples of products similar to the ones I have been working with).
2. Connect the ESP8266 to your computer
3. Run `dmesg` to see what endpoint the device is mapped to.

Example output:
```[ 4580.658044] usb 2-2: new full-speed USB device number 16 using xhci_hcd
[ 4580.787045] usb 2-2: New USB device found, idVendor=1a86, idProduct=7523
[ 4580.787056] usb 2-2: New USB device strings: Mfr=0, Product=2, SerialNumber=0
[ 4580.787061] usb 2-2: Product: USB2.0-Serial
[ 4580.787832] ch341 2-2:1.0: ch341-uart converter detected
[ 4580.788503] usb 2-2: ch341-uart converter now attached to ttyUSB0
```
In this case the ESP8266 is `/dev/ttyUSB0`, from this point this is also what will be used in the examples.

5. Install [esptool](https://github.com/espressif/esptool), download latest micropython [firmware](http://micropython.org/download#esp8266) for the ESP8266, and flash the ESP8266 with this command(make sure the ESP8266 is in PROG mode):
`esptool.py --port /dev/ttyUSB0 --baud 115200 write_flash --flash_size=detect 0 esp8266-20171101-v1.9.3.bin`
(Where esp8266-20171101-v1.9.3.bin is the name of the image)
6. Install [ampy](https://github.com/adafruit/ampy), copy the two scripts that are required over to the ESP8266 with the commands (make sure to set the ESO8266 in UART mode):
`ampy -p /dev/ttyUSB0 -b 115200 put main.py`
`ampy -p /dev/ttyUSB0 -b 115200 put wifimgr.py`
7. Reboot the ESP8266, `main.py` will run on boot. In the current `main.py`, the ESP8266 tries to connect not a network that has been setup. In the case that no network has been set up, or the ESP8266 is not able to connect, it will go into AP mode. When in AP mode, the ESP8266 creates a new wireless network 'EasyButton'. To set up a new wireless connection, connect to the 'EasyButton' network, go to http://192.168.4.1, and set up the desired network using the UI.
8. In this example of `main.py` the ESP8266 sends a http request, to a Hue bridge to toggle a Hue lightbulb. This code can of course be replaced with anything else.
